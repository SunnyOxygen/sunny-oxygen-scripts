#!/usr/bin/python3

import subprocess

def bash(command):
    return subprocess.run(command.split(" "), check=True, stdout=subprocess.PIPE).stdout.decode('ascii')

def connect():
    output = bash("nzbget -D")

def isConnected():
    output = bash("nzbget -L S | grep state")
    return (len(output) > 0)

def disconnect():
    output = bash("nzbget -Q")



