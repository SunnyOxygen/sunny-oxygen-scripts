#include <stdio.h>
#include <time.h>
#include <stdlib.h>
int main(int argc, char *argv[]) {
	const char *greets[5];
	greets[0] = "Ready for takeoff, %s, stay safe!\n";
        greets[1] = "Welcome home, %s!\n";
        greets[2] = "Greetings, %s! Live long and prosper!\n";
        greets[3] = "Winter is coming, %s!\n";
        greets[4] = "Use the force, %s!\n";
	srand(time(NULL));
	if(argc==2){
		printf(greets[(rand() % 5)], argv[1]);
	} else {
		fprintf(stderr, "Usage: %s <name>\n", argv[0]);
		return 1;
	}
	return 0;
}
