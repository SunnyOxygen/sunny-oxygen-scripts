#!/usr/bin/python3

import subprocess

def bash(command):
    return subprocess.run(command.split(" "), stdout=subprocess.PIPE).stdout.decode('ascii')

def connect(country):
    output = bash("nordvpn c " + country)

def isConnected():
    output = bash("nordvpn status")
    return (not "Disconnected" in output)

def disconnect():
    output = bash("nordvpn d")

def getSettings():
    output = bash("nordvpn settings")
    settings = list(filter(None, output.replace("\r", "").strip("-  ").split("\n")))
    return settings

def setSetting(name, state):
    output = bash("nordvpn set "+name+" "+state)

def getCountries():
    output = bash("nordvpn countries")
    countries = list(filter(None, output.replace("\t", "\n").replace("\r", "").strip("-  ").split("\n")))
    return countries
