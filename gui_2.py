#!/usr/bin/python3
import PySimpleGUIQt as sg
import time as t
import nordvpn

sg.change_look_and_feel('BluePurple')

nvpnSettings = nordvpn.getSettings()
settingsLayout = []
for s in nvpnSettings:
    row = []
    name = s.split(":")[0]
    value = s.split(":")[1][1:]
    row.append(sg.Text(name+":"))
    row.append(sg.Text(value, key="text-"+name))
    row.append(sg.InputText(value, key=name, visible=False))
    row.append(sg.Button('Change '+name))
    settingsLayout.append(row)
topLayout = [[sg.Text('Status: '), sg.Text(text='Disconnected', size=(15,1), key='-OUTPUT-', text_color='red')]]
connectLayout=[[sg.Button('Connect'), sg.Button('Disconnect')]]
layout=topLayout+[[sg.Frame('Settings', settingsLayout)]]+connectLayout

menu_def = ['', ['Open', 'Connect', 'Disconnect', 'Exit']]

window = sg.Window('Nordpy but BETTER', layout)
tray = sg.SystemTray(menu=menu_def, filename=r'icon_green.svg')
window_visible = True
window_closed = False
while True:  # Event Loop
    if window_visible:
        event, values = window.read(timeout=1000)
        if event is None:
            window_visible = False
            window_closed = True
            continue
        elif event == "Minimieren":
            window.Hide()
            window_visible = False
            continue
        if(nordvpn.isConnected()):
            window['-OUTPUT-'].update("Connected")
            window['-OUTPUT-'].update(text_color="green")
        else:
            window['-OUTPUT-'].update("Disconnected")
            window['-OUTPUT-'].update(text_color="red")
        if event == "Change Technology":
            valToSet = "wireguard" if values["Technology"] == "OpenVPN" else "openvpn"
            nordvpn.setSetting("technology", valToSet)
            if valToSet == "wireguard":
                window["text-Technology"].update("WireGuard")
                window["Technology"].update("WireGuard")
            else:
                window["text-Technology"].update("OpenVPN")
                window["Technology"].update("OpenVPN")
        if event == "Change Protocol":
            valToSet = "udp" if values["Protocol"] == "TCP" else "tcp"
            nordvpn.setSetting("protocol", valToSet)
            if valToSet == "udp":
                window["text-Protocol"].update("UDP")
                window["Protocol"].update("UDP")
            else:
                window["text-Protocol"].update("TCP")
                window["Protocol"].update("TCP")
        if event == "Change Kill Switch":
            valToSet = "enabled" if values["Kill Switch"] == "disabled" else "disabled"
            nordvpn.setSetting("killswitch", valToSet)
            if valToSet == "enabled":
                window["text-Kill Switch"].update("enabled")
                window["Kill Switch"].update("enabled")
            else:
                window["text-Kill Switch"].update("disabled")
                window["Kill Switch"].update("disabled")
        if event == "Change CyberSec":
            valToSet = "enabled" if values["CyberSec"] == "disabled" else "disabled"
            nordvpn.setSetting("cybersec", valToSet)
            if valToSet == "enabled":
                window["text-CyberSec"].update("enabled")
                window["CyberSec"].update("enabled")
            else:
                window["text-CyberSec"].update("disabled")
                window["CyberSec"].update("disabled")
        if event == "Change Obfuscate":
            valToSet = "enabled" if values["Obfuscate"] == "disabled" else "disabled"
            nordvpn.setSetting("obfuscate", valToSet)
            if valToSet == "enabled":
                window["text-Obfuscate"].update("enabled")
                window["Obfuscate"].update("enabled")
            else:
                window["text-Obfuscate"].update("disabled")
                window["Obfuscate"].update("disabled")
        if event == "Change Notify":
            valToSet = "enabled" if values["Notify"] == "disabled" else "disabled"
            nordvpn.setSetting("notify", valToSet)
            if valToSet == "enabled":
                window["text-Notify"].update("enabled")
                window["Notify"].update("enabled")
            else:
                window["text-Notify"].update("disabled")
                window["Notify"].update("disabled")
        if event == "Change Auto-connect":
            valToSet = "enabled" if values["Auto-connect"] == "disabled" else "disabled"
            nordvpn.setSetting("autoconnect", valToSet)
            if valToSet == "enabled":
                window["text-Auto-connect"].update("enabled")
                window["Auto-connect"].update("enabled")
            else:
                window["text-Auto-connect"].update("disabled")
                window["Auto-connect"].update("disabled")
        if event == "Change DNS":
            valToSet = "enabled" if values["DNS"] == "disabled" else "disabled"
            nordvpn.setSetting("dns", valToSet)
            if valToSet == "enabled":
                window["text-DNS"].update("enabled")
                window["DNS"].update("enabled")
            else:
                window["text-DNS"].update("disabled")
                window["DNS"].update("disabled")
        if event == "Connect":
            nordvpn.connect("Germany")
        if event == "Disconnect":
            nordvpn.disconnect()

    menu_item = tray.Read(timeout=1000)
    if(nordvpn.isConnected()):
        tray.update(filename=r'icon_green.svg')
    else:
        tray.update(filename=r'icon_red.svg')
    if menu_item == "Open" or menu_item == sg.EVENT_SYSTEM_TRAY_ICON_DOUBLE_CLICKED:
        window_visible = True
        if window_closed:
            window_closed = False
            window = sg.Window('Nordpy but BETTER (Restored)', layout)
        else:
            window.UnHide()
    elif menu_item == "Connect":
        print("Connecting")
        nordvpn.connect("Germany")
    elif menu_item == "Disconnect":
        print("Disconnecting")
        nordvpn.disconnect()
    elif menu_item == "Exit":
        window.close()
        break
