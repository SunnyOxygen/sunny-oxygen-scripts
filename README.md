# Sunny-Oxygen-Scripts

### This is a repository for little scripts I wrote or stole from the internet, please do not expect them to work on your system as well

**DISCLAIMER: ALL FILES IN HERE ARE IN A FOLDER I HAVE REFERENCED BY A BASH VARIABLE CALLED $SCRIPTSDIR. IF YOU DON'T WANT TO CREATE SUCH A VARIABLE YOU HAVE TO REPLACE EACH OCCURENCE IN THE FILES WITH THE PATH OF THE FOLDER WHICH CONTAINS THESE FILES**

<br>

## Content
1. reddit (a commandline subreddit image viewer)
2. wallpaper (a commandline unsplash image viewer + downloader)
3. goto-scripts (commandline directoy switcher)
    1. goto
    2. adgt
    3. rmgt
4. greeting (a custom greeting in your shell)
5. short-scripts (a commandline TinyUrl shortener + urlencoder)
    1. short
    2. urlencode
6. selfscan (a commandline scanner for open ports on your system)
7. search (a commandline tool for searching the internet)
8. emoji (a commandline emoji selector)

<br>

<br>


# reddit
A script which will read from the rss-feed of a subreddit and display all images via sxiv

### Requirements:
* [sxiv](https://github.com/muennich/sxiv)
* [dmenu](https://tools.suckless.org/dmenu/)
* [wget](https://linux.die.net/man/1/wget)
* a file called "reddits" in a location of your choice, which contains the names of the subreddits (default is in the folder of this script)

### Usage:
```bash
$ reddit (OPTIONS)(subreddit)
```
#### Options:

```
-l=[], --limit=[]                    -       The Limit of images to download (default=25)
-s=[], --sort=[], --sort-by=[]       -       Sort by hot,top,controversial,new (default=top)
-t=[], --time=[]                     -       Sort by time (default=day)
subreddit                            -       The name of the subreddit (if you don't want the choice dropdown)
```

<br>

<br>


# wallpaper
A script which will download images from unsplash and has the option to save marked photos to your wallpaper folder
Mark images with "m" in sxiv to download them to your Wallpapers-Folder

### Requirements:
* A token for the [unsplash-API](https://unsplash.com/developers)
* [sxiv](https://github.com/muennich/sxiv)
* optionally - a folder called "Wallpapers" in "~/Pictures"

### Usage:
```bash
$ wallpaper (OPTIONS)
```
#### Options:
```
-l=[], --limit=[], -c=[], --count=[] -       The Limit of images to download (default=10)
-q=[], --query=[]                    -       Search for image-topics
-r, --random                         -       Get random images
-f, --featured                       -       Get featured images
```

<br>

<br>


# goto-scripts
A collections of scripts which can be used to mark, remove and jump to marked directories

### Requirements:
* a file called dirs.txt (with absolute paths of directories)
* [dmenu](https://tools.suckless.org/dmenu/)

### Usage:
```bash
$ goto
```
Will open a dmenu list of directories to cd to
```bash
$ adgt
```
Will add current directory to dirs.txt file
```bash
$ rmgt
```
Will open dirs.txt file in dmenu to select line to remove

<br>

<br>


# greeting
A c-script which can be used for example in your bashrc to have a custom greeting as you open a terminal

### Requirements:
* you have to compile the c-script first

<br>

<br>


# short-scripts
A script which will paste a url-string to TinyUrl-API and an alternative source and get the answer (a shortened link) for sharing it easily

### Requirements:
* [curl](https://linux.die.net/man/1/curl)

### Usage:
```bash
$ short [url]
```
```
url - the url to shorten
```
```bash
$ urlencode [url]
```
```
url - the url to encode
```

<br>

<br>


# selfscan
A script which will display all open ports on your system

### Requirements:
* none, i think

### Usage:
```bash
$ selfscan
```

<br>

<br>


# search
A script which will let you search different website

### Requirements:
* [dmenu](https://tools.suckless.org/dmenu/)
* a browser which is represented by the $BROWSER variable
* a file called search_engines.txt which contains the urls of the search engines int the patter NAME [-abbreviation]:URL

### Usage:
```bash
$ search (OPTION|SERVICE) "[search-term]"
```

### OPTION|SERVICE:
```
-h, --help                           -       Display Usage
-l, --list-services                  -       Display Services from search_engines.txt file
-[service-abbreviation]              -       Will open $BROWSER with this service
search-term                          -       The query your searching for (please use quotation marks to search for a phrase)
```

<br>

<br>


# emoji
A script which will open a dmenu prompt to select an emoji a copy it to your clipboard

### Requirements:
* [dmenu](https://tools.suckless.org/dmenu/)
* [xclip](https://linux.die.net/man/1/xclip)
* a file emojis, which contains a list of emojis and names etc
* [notify-send](http://manpages.ubuntu.com/manpages/xenial/man1/notify-send.1.html)

### Usage:
```bash
$ emoji
```
